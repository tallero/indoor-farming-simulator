#	 window.py
#
# Copyright 2020 Pellegrino Prevete
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from datetime import datetime
from gi.repository import Gio, GLib, Gdk, Gtk


@Gtk.Template(resource_path='/ml/arcipelago/truocolo/indoor_farming_simulator/buy_menus.ui')
class IndoorFarmingSimulatorWindow(Gtk.ApplicationWindow):
    __gtype_name__ = 'IndoorFarmingSimulatorWindow'

    BASE_KEY = "ml.arcipelago.truocolo.indoor_farming_simulator"

    convert_couple = lambda self, couple: [GLib.Variant("i", axis) for axis in couple]
    integer_type = GLib.VariantType("i")

    playing = False

    Style = Gtk.Template.Child("Style")
    BuyFrom = Gtk.Template.Child("BuyFrom")
    BuyFor = Gtk.Template.Child("BuyFor")
    Time = Gtk.Template.Child("Time")
    time_of_the_day = Gtk.Template.Child("time_of_the_day")
    current_day = Gtk.Template.Child("current_day")
    current_days = Gtk.Template.Child("current_days")

    def __init__(self, **kwargs):
      super().__init__(**kwargs)

      self.gio_settings = Gio.Settings.new(self.BASE_KEY)

      self.remember_position = self.gio_settings.get_boolean("remember-position")

      self.play = {"style": self.Style,
                   "buyfrom": self.BuyFrom,
                   "buyfor": self.BuyFor,
                   "time": self.Time}

      if self.remember_position:
        self.load_window_state("menu", self)
        self.save_window_state("menu", self)
        self.set_widgets_state()

      GLib.timeout_add(1000, self.clock_widget_action)

    @Gtk.Template.Callback()
    def play_clicked_cb(self, widget, **kwargs):

      if self.playing:
        self.playing = False
        widget.set_label("💮️ Play")
        for win in self.play:
          self.play[win].hide()

      elif not self.playing:
        self.playing = True
        widget.set_label("🌇️ Stand-by")
        for win in self.play:
          self.play[win].show_all()

    def set_widgets_state(self):
      for win in self.play:
        self.load_window_state(win, self.play[win])
        GLib.timeout_add(5000, self.save_window_state, win, self.play[win])

    def clock_widget_action(self, *args):
        now = datetime.now()
        date = now.strftime("%a %-d %B %Y")
        time = now.strftime("%H:%M:%S")
        days = (now - datetime(2020,5,4)).days
        self.current_day.set_label("📅️ " + date)
        self.time_of_the_day.set_label("🕔️ " + time)
        self.current_days.set_label("🌞️ {} days".format(days))
        return True

    def load_window_state(self, name, window, *args):
      try:
        key_name = "{}-size".format(name)
        size = self.gio_settings.get_value(key_name)
        window.set_default_size(*size)

        key_name = "{}-position".format(name)
        position = self.gio_settings.get_value(key_name)
        window.move(*position)
      except Exception as e:
        print(e)

    def save_window_state(self, name, window, *args, display=True):
      # Size
      size = self.convert_couple(window.get_size())
      size = GLib.Variant.new_array(self.integer_type, size)
      key_name = "{}-size".format(name)
      self.gio_settings.set_value(key_name, size)

      # Position
      position = self.convert_couple(window.get_position())
      position = GLib.Variant.new_array(self.integer_type, position)
      key_name = "{}-position".format(name)
      self.gio_settings.set_value(key_name, position)

      return True

    @Gtk.Template.Callback()
    def quit_clicked_cb(self, widget, **kwargs):
      self.destroy()
