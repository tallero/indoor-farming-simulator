# Indoor Farming Simulator

This is a stub for a potential new type of videogame.

[![Indoor Farming Simulator](thumb.png)](https://www.youtube.com/watch?v=h2qDqOQKVlc)

## About

This program is licensed under [GNU Affero General Public License v3 or later](https://www.gnu.org/licenses/agpl-3.0.en.html) 
by [Pellegrino Prevete](http://prevete.ml).<br>
If you find this program useful, consider donating a new [computer](https://patreon.com/tallero) 
or a part time remote [job](mailto:pellegrinoprevete@gmail.com) to help me pay the bills.

Icon is released under [CC-BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/deed.it)
 by [DarKobra](https://commons.wikimedia.org/wiki/File:Tango_icon_nature.svg)

